module Main where

import Utils.Fallible
import Utils.Fallible.Trans
import Utils.Messaging (TerminalColor(..), toTColor)
import Utils.Processes (Failure(..), Result(..), exec, silently, run)

import Control.Exception (tryJust)
import Control.Monad (guard, void)
import Data.Char (isSpace)
import Data.List (findIndex, isPrefixOf, isSuffixOf)
import Data.Map (Map, assocs, foldlWithKey, insert)
import qualified Data.Map as Map (empty, null, lookup)
import System.Environment (getEnv)
import System.IO (hPutStrLn)
import qualified System.IO as System
import System.IO.Error (isDoesNotExistError)
import System.Posix.Directory (getWorkingDirectory)
import System.Process (proc, shell)

-- constants

mainBranchName = "main"

maxBranchNameLength :: Int
maxBranchNameLength = 30

detachedHeadBranchColor = Yellow

mainBranchColor = Green

localBranchColor = Cyan

preferredMaxStatusStringLength :: Int
preferredMaxStatusStringLength = 12

-- general utils

indexed :: ([a] -> [(Int, a)])
indexed = zip [0..]

dropSuffix :: String -> String -> String
dropSuffix toDrop toDropFrom =
	if toDrop `isSuffixOf` toDropFrom then
		take (length toDropFrom - length toDrop) toDropFrom
	else
		toDropFrom

listIf :: [(Bool, a)] -> [a]
listIf conditionalItems = [ item | (conditional, item) <- conditionalItems, conditional ]

-- internal data types

data GitStatusSectionType = Untracked | Staged | Unstaged | Conflicts deriving (Enum, Eq, Ord)

sectionHeader :: GitStatusSectionType -> String
sectionHeader statusSection = case statusSection of
	Untracked -> "Untracked files"
	Staged -> "Changes to be committed"
	Unstaged -> "Changes not staged for commit"
	Conflicts -> "Unmerged paths"

sectionColored :: GitStatusSectionType -> Bool
sectionColored statusSection = statusSection `elem` [Staged, Conflicts]

data GitFileStatus = NewFile | Modified | Renamed | Deleted | BothModified | BothAdded | DeletedByUs | DeletedByThem deriving (Enum, Eq, Ord)

statusMarker :: GitFileStatus -> String
statusMarker fileStatus = case fileStatus of
	NewFile -> "new file"
	Modified -> "modified"
	Renamed -> "renamed"
	Deleted -> "deleted"
	BothModified -> "both modified"
	BothAdded -> "both added"
	DeletedByUs -> "deleted by us"
	DeletedByThem -> "deleted by them"

stagedStatusColor :: GitFileStatus -> TerminalColor
stagedStatusColor status = case status of
	NewFile -> Green
	Modified -> Magenta
	Renamed -> Magenta
	Deleted -> Yellow
	BothModified -> Red
	BothAdded -> Red
	DeletedByUs -> Red
	DeletedByThem -> Red

fileStatusChar :: GitFileStatus -> Char
fileStatusChar status = case status of
	NewFile -> '+'
	Modified -> '*'
	Renamed -> '>'
	Deleted -> '-'
	BothModified -> '%'
	BothAdded -> '&'
	DeletedByUs -> '/'
	DeletedByThem -> '\\'

type CommitHash = String

data BranchData = Rebasing String CommitHash | CherryPicking String CommitHash | DetachedHead String | LocalBranch String

branchTerminalPS1 :: Bool -> BranchData -> String
branchTerminalPS1 _ (Rebasing branchName rebasingCommit) =
	tColorPS1 [localBranchColor, Invert, Bold] branchName ++
		tColorPS1 [RedBG, Yellow, Bold] "->" ++
		tColorPS1 [RedBG, Bold, ResetFG] rebasingCommit
branchTerminalPS1 _ (CherryPicking branchName cherryPickCommit) =
	tColorPS1 [RedBG, Bold, ResetFG] cherryPickCommit ++
		tColorPS1 [RedBG, Yellow, Bold] "->" ++
		tColorPS1 [localBranchColor, YellowBG, Invert, Bold] branchName
branchTerminalPS1 hasChanges (DetachedHead branchName) = do
	let colorsForChanges = if hasChanges then [Invert, Bold] else []
	let mainBranchSuffix = '/':mainBranchName
	if mainBranchSuffix `isSuffixOf` branchName then
		tColorPS1 (Yellow:colorsForChanges) (dropSuffix mainBranchSuffix branchName) ++
			tColorPS1 (Reset:colorsForChanges) "/" ++
			tColorPS1 (mainBranchColor:colorsForChanges) mainBranchName
	else
		tColorPS1 (Yellow:colorsForChanges) branchName
branchTerminalPS1 hasChanges (LocalBranch branchName) = do
	let branchColor = if branchName == mainBranchName then mainBranchColor else localBranchColor
	tColorPS1 ([branchColor, Bold] ++ listIf [(hasChanges, Invert)]) branchName

type GitStatusSection = Map GitFileStatus [FilePath]
type GitSectionsData = Map GitStatusSectionType GitStatusSection
data GitStatus = GitStatus {
	branchData :: BranchData,
	statusData :: GitSectionsData }

-- stateless implementation

tColorPS1 :: [TerminalColor] -> String -> String
tColorPS1 colors message = do
	let toTColorPS1 = ("\\["++) . (++"\\]") . toTColor
	toTColorPS1 colors ++ message ++ toTColorPS1 [Reset]

interpretFileLineContent :: String -> Int -> Fallible (GitFileStatus, FilePath)
interpretFileLineContent line firstColonIndex = do
	let statusMarkerString = take firstColonIndex line
	let matchingStatusMarkers = filter ((statusMarkerString ==) . statusMarker) $ enumFrom NewFile
	if length matchingStatusMarkers /= 1 then
		Error ((show $ length matchingStatusMarkers) ++ " status markers matched the file status marker found: \"" ++ statusMarkerString ++ "\"!")
	else do
		let matchingStatusMarker = head matchingStatusMarkers
		Falue (matchingStatusMarker, dropWhile isSpace $ drop (firstColonIndex + 1) line)

readFileLineContent :: GitStatusSectionType -> String -> Fallible (GitFileStatus, FilePath)
readFileLineContent currentSection line =
	if currentSection == Untracked then
		Falue (NewFile, line)
	else do
		maybe (Error "This supposed file line has no colon to separate the file's status from the file path!")
			(interpretFileLineContent line)
			$ findIndex (':'==) line

readFileLine :: String -> GitStatusSectionType -> GitSectionsData -> Fallible GitSectionsData
readFileLine line currentSection gitStatus = do
	-- TODO: if an untracked file is a folder, don't just ad dit as one file; add all files contained within it
	(fileStatus, filePath) <- readFileLineContent currentSection $ tail line
	let oldSectionInfo = maybe Map.empty id $ Map.lookup currentSection gitStatus
	let oldFiles = maybe [] id $ Map.lookup fileStatus oldSectionInfo
	let newFiles = oldFiles ++ [filePath]
	let newSectionInfo = insert fileStatus newFiles oldSectionInfo
	Falue $ insert currentSection newSectionInfo gitStatus

gitStatusPieceString :: GitFileStatus -> Int -> Bool -> String
gitStatusPieceString fileStatus numberOfFiles countOnly = do
	let char = fileStatusChar fileStatus
	let fileStatusPS1 = if char == '\\' then "\\\\" else [char]
	if countOnly then
		fileStatusPS1 ++ show numberOfFiles
	else
		concat $ take numberOfFiles $ repeat fileStatusPS1

gitStatusColor :: GitStatusSectionType -> GitFileStatus -> TerminalColor
gitStatusColor section fileStatus =
	if sectionColored section
		then stagedStatusColor fileStatus
		else LightGray

gitStatusPiece :: GitStatusSectionType -> GitFileStatus -> Int -> Bool -> String
gitStatusPiece section fileStatus numberOfFiles countOnly =
	tColorPS1 [gitStatusColor section fileStatus, Invert] $
		gitStatusPieceString fileStatus numberOfFiles countOnly

findLongestShrinkablePS1Index :: [(Int, Bool)] -> Fallible Int
findLongestShrinkablePS1Index statusCounts = do
	let indexedShrinkableLengths = filter (\(_, (length, countOnly)) -> not countOnly && length > 2) $ indexed statusCounts
	let getLength = fst . snd
	let longestLength = maximum $ map getLength indexedShrinkableLengths
	let longestShrinkableLengthIndices = map fst $ filter ((longestLength==) . getLength) indexedShrinkableLengths
	ifElseError (not $ null longestShrinkableLengthIndices)
		(head longestShrinkableLengthIndices)
		"All git section PS1s have reached their minimal length!"

-- TODO: There seems to be a bug here; it doesn't always end up as short as expected.
calculateShortenNeeds' :: GitStatusSectionType -> [(Int, Bool)] -> [Bool]
calculateShortenNeeds' sectionType statusSectionPS1Lengths =
	if (foldl (+) 0 $ map fst statusSectionPS1Lengths) > preferredMaxStatusStringLength then do
		let longestShrinkablePS1IndexOrError = findLongestShrinkablePS1Index statusSectionPS1Lengths
		let recalculateShortenNeeds = \longestShrinkablePS1Index -> do
			let updateStatusSectionPS1Length = \(index, oldLengthAndCount@(oldLength, _)) ->
				if longestShrinkablePS1Index == index then
					((oldLength `div` 10) + 1, True)
				else
					oldLengthAndCount
			let newStatusSectionPS1Lengths = map updateStatusSectionPS1Length $ indexed statusSectionPS1Lengths
			calculateShortenNeeds' sectionType newStatusSectionPS1Lengths
		let allCountOnly = take (length statusSectionPS1Lengths) $ repeat True
		fallible (const allCountOnly) recalculateShortenNeeds longestShrinkablePS1IndexOrError
	else
		map snd statusSectionPS1Lengths

calculateShortenNeeds :: GitStatusSectionType -> [Int] -> [Bool]
calculateShortenNeeds sectionType statusSectionPS1Lengths =
	calculateShortenNeeds' sectionType $ zip statusSectionPS1Lengths $ repeat False

gitStatusSectionPS1 :: GitStatusSectionType -> GitStatusSection -> String
gitStatusSectionPS1 sectionType sectionData = do
	let fullGitStatusSectionPS1Lengths = map (\(fileStatus, files) -> length $ gitStatusPieceString fileStatus (length files) False) $ assocs sectionData
	let countOnlys = calculateShortenNeeds sectionType fullGitStatusSectionPS1Lengths
	let gitStatusPieces = map (\(index, (fileStatus, files)) -> gitStatusPiece sectionType fileStatus (length files) (countOnlys !! index)) $ indexed $ assocs sectionData
	foldl (++) [] gitStatusPieces

gitStatusToPS1 :: GitStatus -> String
gitStatusToPS1 gitStatus = do
	let hasChanges = not $ Map.null $ statusData gitStatus

	let displayBranch = "*" ++ branchTerminalPS1 hasChanges (branchData gitStatus)

	let changes = foldlWithKey (\gitStatusPS1' sectionType section -> gitStatusPS1' ++ gitStatusSectionPS1 sectionType section) "" $ statusData gitStatus

	displayBranch ++ changes

data ReadGitStatus'State = ReadGitStatus'State {
	maybeBranchData :: Maybe BranchData,
	gitSectionsData :: GitSectionsData,
	remainingLines :: [String],
	maybeCurrentSection :: Maybe GitStatusSectionType }

readGitStatus' :: ReadGitStatus'State -> Fallible GitStatus
readGitStatus' state@ReadGitStatus'State{remainingLines=[]} =
	fromMaybe "No branch name was found from `git status`!"
		$ fmap (\branchData -> GitStatus { branchData = branchData, statusData = gitSectionsData state })
		$ maybeBranchData state
readGitStatus' state@ReadGitStatus'State{remainingLines=([]:nextLines)} = readGitStatus' state{ remainingLines = nextLines }
readGitStatus' state@ReadGitStatus'State{remainingLines=(line:nextLines)} = do
	let nextLineState = state{ remainingLines = nextLines }
	if "You are currently rebasing branch '" `isPrefixOf` line then do
		let atRebasingBranchName = drop 35 line
		let (rebasingBranchName, afterRebasingBranchName) = span (/='\'') atRebasingBranchName
		let atRebasingCommitHash = drop 6 afterRebasingBranchName  -- drop "' on '"
		let rebasingCommitHash = takeWhile (/='\'') atRebasingCommitHash
		readGitStatus' nextLineState{ maybeBranchData = Just $ Rebasing rebasingBranchName rebasingCommitHash }
	else if "You are currently cherry-picking commit " `isPrefixOf` line then
		case maybeBranchData state of
			Just (LocalBranch branchName) -> readGitStatus' nextLineState{ maybeBranchData = Just $ CherryPicking branchName (drop 40 $ dropSuffix "." line) }
			_ -> Error "This name of the current branch could not be found during a cherry-pick!"
	else if "On branch " `isPrefixOf` line then
		readGitStatus' nextLineState{ maybeBranchData = Just $ LocalBranch $ drop 10 line }
	else if "HEAD detached at " `isPrefixOf` line then
		readGitStatus' nextLineState{ maybeBranchData = Just $ DetachedHead $ drop 17 line }
	else if "HEAD detached from " `isPrefixOf` line then
		readGitStatus' nextLineState{ maybeBranchData = Just $ DetachedHead $ drop 19 line }
	else if head line == '\t' then
		-- read file line
		maybe (Error ("I found a line that looked like a file, but it was not in a file section: \"" ++ line ++ "\"!"))
			(\currentSection -> do
				newSectionsData <- readFileLine line currentSection (gitSectionsData state)
				readGitStatus' nextLineState{ gitSectionsData = newSectionsData })
			$ maybeCurrentSection state
	else if last line == ':' then do
		-- read section header
		let matchingSections = filter ((init line==) . sectionHeader) $ enumFrom Untracked
		if length matchingSections == 1 then
			readGitStatus' nextLineState{ maybeCurrentSection = Just $ head matchingSections }
		else if length matchingSections > 1 then
			Error ("There were " ++ (show $ length matchingSections) ++ " sections matching the section header \"" ++ line ++ "\"!")
		else
			readGitStatus' nextLineState
	else
		readGitStatus' nextLineState

-- stateful implementation

setTerminalWindowName :: String -> Void
setTerminalWindowName name = elseErr $ void $ exec "set Alacritty window name" $
	shell $ "xdotool set_window --name '" ++ name ++ "' \"$ALACRITTY_WINDOW_ID\""

readGitStatus :: IOFallible (Maybe GitStatus)
readGitStatus = do
	result <- run "git status" $ silently $
		proc "git" ["-c", "color.status=never", "status"]
	case result.error of
		Nothing -> falT $ fmap Just $ readGitStatus' ReadGitStatus'State {
			maybeBranchData = Nothing,
			gitSectionsData = Map.empty,
			remainingLines = lines result.stdout,
			maybeCurrentSection = Nothing }
		Just Failure{code=128} -> -- Not a git repo
			return Nothing
		Just Failure{code} -> errorT $ "PromptMaker's git status failed with exit code " ++ show code ++ "!"

useHomeTilde :: FilePath -> IO FilePath
useHomeTilde path = do
	homeDirOrVoid <- tryJust (guard . isDoesNotExistError) $ getEnv "HOME"
	let useHomeTilde' = \homeDir ->
		if homeDir == path then
			"~"
		else if (homeDir ++ "/") `isPrefixOf` path then
			"~/" ++ drop (length homeDir + 1) path
		else
			path
	return $ either (const path) useHomeTilde' homeDirOrVoid

makeWorkingDirDisplay :: IO String
makeWorkingDirDisplay = do
	workingDir <- getWorkingDirectory
	tildedWorkingDir <- useHomeTilde workingDir
	return $ dropSuffix "/" $ tildedWorkingDir

makePS1 :: IO String
makePS1 = do
	displayWorkingDir <- makeWorkingDirDisplay
	maybeGitStatusOrError <- falM readGitStatus
	gitStatusPS1 <- case maybeGitStatusOrError of
		Error error -> do
			hPutStrLn System.stderr $ "There was an issue attempting to read this git status: " ++ error
			return ""
		Falue Nothing -> return ""
		Falue (Just gitStatus) -> return $ gitStatusToPS1 gitStatus

	return $ tColorPS1 [Blue, Bold] displayWorkingDir ++ gitStatusPS1 ++ " \\$ "

-- TODO: find out if https://hackage.haskell.org/package/ncurses can be used to run `git status` when you click on the changes string
main = do
	workingDirDisplay <- makeWorkingDirDisplay
	setTerminalWindowName workingDirDisplay

	newPS1 <- makePS1
	putStrLn newPS1
