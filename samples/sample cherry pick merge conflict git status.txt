On branch physical-ordering
You are currently cherry-picking commit 75db8031.
  (fix conflicts and run "git cherry-pick --continue")
  (use "git cherry-pick --abort" to cancel the cherry-pick operation)

Changes to be committed:

	modified:   catalog/gsr-ordering/pom.xml
	new file:   catalog/gsr-ordering/src/main/java/mil/nga/catalog/gsrordering/FtpDeliveryService.java
	modified:   catalog/gsr-ordering/src/main/java/mil/nga/catalog/gsrordering/GsrOrderingConnection.java
	new file:   catalog/gsr-ordering/src/main/java/mil/nga/catalog/gsrordering/PhysicalDeliveryService.java
	modified:   catalog/gsr-ordering/src/main/resources/OSGI-INF/blueprint/blueprint.xml
	modified:   catalog/gsr-ordering/src/main/resources/OSGI-INF/metatype/metatype.xml
	renamed:    catalog/gsr-ordering/src/test/java/mil/nga/catalog/gsrordering/GsrOrderingDeliveryServiceTest.java -> catalog/gsr-ordering/src/test/java/mil/nga/catalog/gsrordering/FtpDeliveryServiceTest.java
	new file:   catalog/gsr-ordering/src/test/java/mil/nga/catalog/gsrordering/PhysicalDeliveryServiceTest.java
	renamed:    catalog/gsr-ordering/src/test/resources/mock-gsr-ordering/mappings/CreateNewOrder.json -> catalog/gsr-ordering/src/test/resources/mock-gsr-ordering/mappings/CreateNewFtpOrder.json
	new file:   catalog/gsr-ordering/src/test/resources/mock-gsr-ordering/mappings/CreateNewPhysicalOrder.json
	new file:   distribution/test/itests/test-itests-common/src/main/java/mil/nga/gsr/test/itests/common/OrderServiceStub.java
	modified:   distribution/test/itests/test-itests-gsr/pom.xml
	new file:   distribution/test/itests/test-itests-gsr/src/test/java/mil/nga/gsr/test/itests/TestOrdering.java
	new file:   distribution/test/itests/test-itests-gsr/src/test/resources/sample_ncl_1.xml

Unmerged paths:
  (use "git add <file>..." to mark resolution)

	both modified:   catalog/gsr-ordering/src/main/java/mil/nga/catalog/gsrordering/GsrOrderingDeliveryService.java

